extends KinematicBody2D

#TODO: améliorer les animations

enum DIRECTION { UP, DOWN, RIGHT, LEFT }

var direction = DIRECTION.DOWN
var spawn_pos
var damages = 2
var health = 10
var speed = 60
var lives = 8 # 8 + 1 (first spawn) = 9
var invincibility = false
var invincibility_duration_progress = 0
var label_lives_visibility_progress = 0
export var invincibility_duration = 0.5
export var label_lives_visibility_duration = 2

onready var animator = get_node("AnimationPlayer")
onready var hitzone = get_node("hitzone")
onready var label_lives = get_node("Label")

func _ready():
	spawn_pos = get_global_pos()
	label_lives.set_text(String(lives+1)+" lives left")
	label_lives.set_hidden(false)
	set_process(true)

func _process(delta):
	check_invincibility_duration(delta)
	check_label_lives_visibility_duration(delta)

func suffer_damages(damages):
	if !invincibility:
		health -= damages
		check_is_alive()
		activate_invincibility()

func check_is_alive():
	if health <= 0:
		death()

func check_invincibility_duration(delta):
	if invincibility == true:
		if invincibility_duration_progress < invincibility_duration:
			invincibility_duration_progress += delta
		else:
			deactivate_invincibility()

func death():
	if lives <= 0:
		get_tree().change_scene("res://scenes/menus/Game_Over_menu/Game_Over_menu.tscn")
	else:
		health = 10
		set_global_pos(spawn_pos)
		lives -= 1
		show_label_lives()

func activate_invincibility():
	invincibility = true
	set_opacity(0.5)

func deactivate_invincibility():
	invincibility = false
	invincibility_duration_progress = 0
	set_opacity(1.0)

func _enter_tree():
	g_player.player = self

func _exit_tree():
	g_player.player = null

func check_label_lives_visibility_duration(delta):
	if label_lives.is_visible():
		if label_lives_visibility_progress < label_lives_visibility_duration:
			label_lives_visibility_progress += delta
		else:
			hide_label_lives()

func hide_label_lives():
	label_lives_visibility_progress = 0
	label_lives.set_hidden(true)

func show_label_lives():
	label_lives.set_text(String(lives+1)+" lives left")
	label_lives.set_hidden(false)