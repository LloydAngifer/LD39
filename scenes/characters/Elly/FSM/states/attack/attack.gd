extends "res://addons/net.kivano.fsm/content/FSMState.gd";
################################### R E A D M E ##################################
# For more informations check script attached to FSM node
#
#

##################################################################################
#####  Variables (Constants, Export Variables, Node Vars, Normal variables)  #####
######################### var myvar setget myvar_set,myvar_get ###################

##################################################################################
#########                       Getters and Setters                      #########
##################################################################################
#you will want to use those
func getFSM(): return fsm; #defined in parent class
func getLogicRoot(): return logicRoot; #defined in parent class 

##################################################################################
#########                 Implement those below ancestor                 #########
##################################################################################
#you can transmit parameters if fsm is initialized manually
func stateInit(inParam1=null,inParam2=null,inParam3=null,inParam4=null, inParam5=null): 
	pass

#when entering state, usually you will want to reset internal state here somehow
func enter(fromStateID=null, fromTransitionID=null, inArg0=null,inArg1=null, inArg2=null):
	pass

#when updating state, paramx can be used only if updating fsm manually
func update(deltaTime, param0=null, param1=null, param2=null, param3=null, param4=null):
	var player_down = logicRoot.direction == logicRoot.DIRECTION.DOWN
	var player_up = logicRoot.direction == logicRoot.DIRECTION.UP
	var player_right = logicRoot.direction == logicRoot.DIRECTION.RIGHT
	var player_left = logicRoot.direction == logicRoot.DIRECTION.LEFT

	if player_down:
		logicRoot.animator.play("attack_down")
	if player_up:
		logicRoot.animator.play("attack_up")
	if player_right:
		logicRoot.animator.play("attack_right")
	if player_left:
		logicRoot.animator.play("attack_left")
	
	var targets = logicRoot.hitzone.get_overlapping_bodies()
	for target in targets:
		if target.is_in_group("Living"):
			target.suffer_damages(logicRoot.damages)

#when exiting state
func exit(toState=null):
	pass

##################################################################################
#########                       Connected Signals                        #########
##################################################################################

##################################################################################
#########     Methods fired because of events (usually via Groups interface)  ####
##################################################################################

##################################################################################
#########                         Public Methods                         #########
##################################################################################

##################################################################################
#########                         Inner Methods                          #########
##################################################################################

##################################################################################
#########                         Inner Classes                          #########
##################################################################################
