extends "res://addons/net.kivano.fsm/content/FSMState.gd";
################################### R E A D M E ##################################
# For more informations check script attached to FSM node
#
#

##################################################################################
#####  Variables (Constants, Export Variables, Node Vars, Normal variables)  #####
######################### var myvar setget myvar_set,myvar_get ###################

##################################################################################
#########                       Getters and Setters                      #########
##################################################################################
#you will want to use those
func getFSM(): return fsm; #defined in parent class
func getLogicRoot(): return logicRoot; #defined in parent class 

##################################################################################
#########                 Implement those below ancestor                 #########
##################################################################################
#you can transmit parameters if fsm is initialized manually
func stateInit(inParam1=null,inParam2=null,inParam3=null,inParam4=null, inParam5=null): 
	pass

#when entering state, usually you will want to reset internal state here somehow
func enter(fromStateID=null, fromTransitionID=null, inArg0=null,inArg1=null, inArg2=null):
	pass

#when updating state, paramx can be used only if updating fsm manually
func update(deltaTime, param0=null, param1=null, param2=null, param3=null, param4=null):
	var player_down = Input.is_action_pressed("player_down")
	var player_up = Input.is_action_pressed("player_up")
	var player_right = Input.is_action_pressed("player_right")
	var player_left = Input.is_action_pressed("player_left")
	var velocity = Vector2(0,0)
	
	if player_down:
		logicRoot.direction = logicRoot.DIRECTION.DOWN
		logicRoot.hitzone.set_pos(Vector2(-0.75,22.79))
		velocity += Vector2(0,1)
	if player_up:
		logicRoot.direction = logicRoot.DIRECTION.UP
		logicRoot.hitzone.set_pos(Vector2(0.31,-22.92))
		velocity += Vector2(0,-1)
	if player_right:
		logicRoot.direction = logicRoot.DIRECTION.RIGHT
		logicRoot.hitzone.set_pos(Vector2(22.63,0.47))
		velocity += Vector2(1,0)
	if player_left:
		logicRoot.direction = logicRoot.DIRECTION.LEFT
		logicRoot.hitzone.set_pos(Vector2(-22.43,0.04))
		velocity += Vector2(-1,0)
	
	if logicRoot.direction == logicRoot.DIRECTION.DOWN:
		logicRoot.animator.play("walk_down")
	if logicRoot.direction == logicRoot.DIRECTION.UP:
		logicRoot.animator.play("walk_up")
	if logicRoot.direction == logicRoot.DIRECTION.RIGHT:
		logicRoot.animator.play("walk_right")
	if logicRoot.direction == logicRoot.DIRECTION.LEFT:
		logicRoot.animator.play("walk_left")
	
	var rest = logicRoot.move(velocity*deltaTime*logicRoot.speed)
	if logicRoot.is_colliding() == true:
		var normal = logicRoot.get_collision_normal()
		rest = normal.slide(rest)
		logicRoot.move(rest)

#when exiting state
func exit(toState=null):
	pass

##################################################################################
#########                       Connected Signals                        #########
##################################################################################

##################################################################################
#########     Methods fired because of events (usually via Groups interface)  ####
##################################################################################

##################################################################################
#########                         Public Methods                         #########
##################################################################################

##################################################################################
#########                         Inner Methods                          #########
##################################################################################

##################################################################################
#########                         Inner Classes                          #########
##################################################################################
