tool
extends "res://addons/net.kivano.fsm/content/FSMTransition.gd";
################################### R E A D M E ##################################
# For more informations check script attached to FSM node
# All params are optional and will be used only if you decide to manually initialize FSM (fsm.init())
# or update manually (fsm.update(deltaTime))
#
# You can also use accomplish() method on this transition to mark it as accomplised until next related 
# state activation (you can use it from code, or connect some signals to accomplish() method)

##################################################################################
#####  Variables (Constants, Export Variables, Node Vars, Normal variables)  #####
######################### var myvar setget myvar_set,myvar_get ###################

######################################
####### Getters
func getFSM(): return fsm; #access to owner FSM, defined in parent class
func getLogicRoot(): return logicRoot; #access to logic root of FSM (usually fsm.get_parent())

######################################
####### Implement those below ########
func transitionInit(inParam1=null, inParam2=null, inParam3=null, inParam4=null, inParam5=null): 
	#you can optionally implement this to initialize transition on it's creation time 
	pass

func prepare(inNewStateID): 
	#you can optionally implement this to reset transition when related state has been activated
	pass

func transitionCondition(inDeltaTime, inParam0=null, inParam1=null, inParam2=null, inParam3=null, inParam4=null): 
	var player_down = Input.is_action_pressed("player_down")
	var player_up = Input.is_action_pressed("player_up")
	var player_right = Input.is_action_pressed("player_right")
	var player_left = Input.is_action_pressed("player_left")
	
	if !(player_down || player_up || player_right || player_left):
		if logicRoot.direction == logicRoot.DIRECTION.DOWN:
			logicRoot.animator.play("idle_down")
		if logicRoot.direction == logicRoot.DIRECTION.UP:
			logicRoot.animator.play("idle_up")
		if logicRoot.direction == logicRoot.DIRECTION.RIGHT:
			logicRoot.animator.play("idle_right")
		if logicRoot.direction == logicRoot.DIRECTION.LEFT:
			logicRoot.animator.play("idle_left")
		return true
	
	return false;
