extends KinematicBody2D

#TODO: améliorer les animations

enum DIRECTION { UP, DOWN, RIGHT, LEFT }

signal death

var direction = DIRECTION.DOWN
var damages = 1
var health = 10
var speed = 50
var invincibility = false
var invincibility_duration_progress = 0
export var invincibility_duration = 0.5

onready var animator = get_node("AnimationPlayer")

func _ready():
	pass

func _process(delta):
	check_invincibility_duration(delta)

func suffer_damages(damages):
	if !invincibility:
		health -= damages
		check_is_alive()
		activate_invincibility()

func check_is_alive():
	if health <= 0:
		death()

func check_invincibility_duration(delta):
	if invincibility_duration_progress < invincibility_duration:
		invincibility_duration_progress += delta
	else:
		deactivate_invincibility()

func death():
	emit_signal("death")
	queue_free()

func activate_invincibility():
	invincibility = true
	set_opacity(0.5)
	set_process(true)

func deactivate_invincibility():
	invincibility = false
	invincibility_duration_progress = 0
	set_opacity(1.0)
	set_process(false)