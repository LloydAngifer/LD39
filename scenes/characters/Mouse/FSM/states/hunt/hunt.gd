extends "res://addons/net.kivano.fsm/content/FSMState.gd";
################################### R E A D M E ##################################
# For more informations check script attached to FSM node
#
#

##################################################################################
#####  Variables (Constants, Export Variables, Node Vars, Normal variables)  #####
######################### var myvar setget myvar_set,myvar_get ###################

##################################################################################
#########                       Getters and Setters                      #########
##################################################################################
#you will want to use those
func getFSM(): return fsm; #defined in parent class
func getLogicRoot(): return logicRoot; #defined in parent class 

##################################################################################
#########                 Implement those below ancestor                 #########
##################################################################################
#you can transmit parameters if fsm is initialized manually
func stateInit(inParam1=null,inParam2=null,inParam3=null,inParam4=null, inParam5=null): 
	pass

#when entering state, usually you will want to reset internal state here somehow
func enter(fromStateID=null, fromTransitionID=null, inArg0=null,inArg1=null, inArg2=null):
	pass

#when updating state, paramx can be used only if updating fsm manually
func update(deltaTime, param0=null, param1=null, param2=null, param3=null, param4=null):
	var navigation_group = get_tree().get_nodes_in_group("Navigation")
	if !navigation_group.empty():
		var navigation = navigation_group.front()
		var path = navigation.get_simple_path(logicRoot.get_global_pos(), get_node("/root/g_player").player.get_global_pos())
		var path_array = Array(path)
		if path_array.empty():
			return
		path_array.remove(0) # This is the actual pos of the mouse
		var direction = path_array.front() - logicRoot.get_global_pos()
		direction = direction.normalized()
		var rest = logicRoot.move(direction*deltaTime*logicRoot.speed)
		
		if logicRoot.is_colliding():
			var normal = logicRoot.get_collision_normal()
			var rest = normal.slide(rest)
			logicRoot.move(rest)
		
		if direction.y > 0 :
			logicRoot.direction = logicRoot.DIRECTION.DOWN
		if direction.y < 0 :
			logicRoot.direction = logicRoot.DIRECTION.UP
		if direction.x > 0.5 :
			logicRoot.direction = logicRoot.DIRECTION.RIGHT
		if direction.x < -0.5 :
			logicRoot.direction = logicRoot.DIRECTION.LEFT
	else:
		print("Group Navigation is empty")
	
	if logicRoot.direction == logicRoot.DIRECTION.DOWN:
		logicRoot.animator.play("walk_down")
	if logicRoot.direction == logicRoot.DIRECTION.UP:
		logicRoot.animator.play("walk_up")
	if logicRoot.direction == logicRoot.DIRECTION.RIGHT:
		logicRoot.animator.play("walk_right")
	if logicRoot.direction == logicRoot.DIRECTION.LEFT:
		logicRoot.animator.play("walk_left")

#when exiting state
func exit(toState=null):
	pass

##################################################################################
#########                       Connected Signals                        #########
##################################################################################

##################################################################################
#########     Methods fired because of events (usually via Groups interface)  ####
##################################################################################

##################################################################################
#########                         Public Methods                         #########
##################################################################################

##################################################################################
#########                         Inner Methods                          #########
##################################################################################

##################################################################################
#########                         Inner Classes                          #########
##################################################################################
