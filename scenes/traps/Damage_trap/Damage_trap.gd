extends Area2D

export var damages = 5

func _ready():
	connect("body_enter", self, "on_body_enter")

func on_body_enter(body):
	if body.is_in_group("Living"):
		body.suffer_damages(damages)
		queue_free()